#!/usr/bin/env node
(() => {
    if (!process.argv[2]) return console.error('Nie podałeś frazy!');

    const phrase = process.argv.slice(2, process.argv.length).join(' ');

    const UA =
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36';
    const puppeteer = require('puppeteer');

    async function uniBoldy(page) {
        return await page.evaluate(() => {
            let agreeButton = Array.from(
                document.querySelectorAll('button')
            ).find((b) => b.innerText == 'Zgadzam się');
            if (agreeButton !== undefined) agreeButton.click();
            return {
                hasNext: document.getElementById('pnnext') ? true : false,
                boldy: Array.from(document.querySelectorAll('em')).map((em) =>
                    em.innerText.toLocaleLowerCase()
                )
            };
        });
    }

    (async () => {
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        await page.setUserAgent(UA);

        await page.setViewport({ width: 1440, height: 685 });

        await page.goto(
            `https://www.google.pl/search?q=${encodeURIComponent(
                phrase
            ).replace(
                /%20/g,
                '+'
            )}&uule=w+CAIQICIGUG9sYW5k&pws=0&filter=0&num=100`,
            { waitUntil: 'networkidle0' }
        );

        let keys = {};

        let { boldy, hasNext } = await uniBoldy(page);

        for (let bold of boldy) {
            if (keys[bold]) keys[bold]++;
            else keys[bold] = 1;
        }

        while (hasNext) {
            await page.click('#pnnext');
            await page.waitForNavigation();

            await page.waitForTimeout(1000 + Math.round(Math.random() * 3000));

            let result = await uniBoldy(page);

            boldy = result.boldy;
            hasNext = result.hasNext;

            for (let bold of boldy) {
                if (keys[bold]) keys[bold]++;
                else keys[bold] = 1;
            }

            await page.waitForTimeout(1000 + Math.round(Math.random() * 3000));
        }

        console.log(
            Object.keys(keys)
                .map((k) => `${k} x${keys[k]}`)
                .join('\n')
        );

        await browser.close();
    })();
})();
